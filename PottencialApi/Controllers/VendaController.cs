using Microsoft.AspNetCore.Mvc;
using PottencialApi.Models.Vendas;
using PottencialApi.Services.Vendas;

namespace PottencialApi.Controllers;

[ApiController]
[Route("api/[controller]")]
public class VendaController : ControllerBase
{
    public readonly IVendaService _vendaService;

    public VendaController(IVendaService vendaService)
    {
        _vendaService = vendaService;
    }

    [HttpGet("{id:int}", Name = "BuscarVenda")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<VendaGet>> BuscarVenda(int id)
    {
        var venda = await _vendaService.GetById(id);
        return Ok(venda);
    }

    [HttpPost(Name = "RegistrarVenda")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> RegistrarVenda(VendaCreate model)
    {
        await _vendaService.Create(model);
        return Ok(new {message = "Venda Registrada"});
    }

    [HttpPut(Name = "AtualizarVenda")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> AtualizarVenda(VendaUpdateStatus model)
    {
        await _vendaService.UpdateStatusVenda(model);
        return Ok(new {message = "Venda Atualizada"});
    }

}
