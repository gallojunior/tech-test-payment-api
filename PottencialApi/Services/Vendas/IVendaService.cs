using PottencialApi.Models.Vendas;

namespace PottencialApi.Services.Vendas;

public interface IVendaService
{
    Task<List<VendaGet>> GetAll();
    Task<VendaGet> GetById(int id);
    Task Create(VendaCreate model);
    Task UpdateStatusVenda(VendaUpdateStatus model);
}
