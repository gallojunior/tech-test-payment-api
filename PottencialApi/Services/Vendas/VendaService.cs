using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PottencialApi.Data;
using PottencialApi.Entities;
using PottencialApi.Helpers;
using PottencialApi.Models.Vendas;

namespace PottencialApi.Services.Vendas
{
    public class VendaService : IVendaService
    {
        private readonly ApplicationDbContext _db;
        private readonly IMapper _mapper;

        public VendaService(ApplicationDbContext db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        public async Task Create(VendaCreate model)
        {
            // Validação dos dados da Venda - Pode ser refatorado em outros serviços
            if (_db.Vendedores.Find(model.VendedorId) == null)
            {
                throw new AppException("O Vendedor de Id " + model.VendedorId + " não existe");
            }

            if (model.ItemVenda.Count == 0)
            {
                throw new AppException("É necessário especificar ao menos 1 produto para venda");
            }

            //Validação dos dados dos Itens Vendidos - Pode ser refatorada em outros serviços
            foreach (var item in model.ItemVenda)
            {
                if (_db.Produtos.Find(item.ProdutoId) == null)
                {
                    throw new AppException("O Produto de Id " + item.ProdutoId + " não existe");
                }
            }

            // Inclusão da venda
            var itemVenda = _mapper.Map<List<ItemVenda>>(model.ItemVenda);
            var venda = _mapper.Map<Venda>(model);
            venda.StatusVendaId = 1;
            venda.DataVenda = DateTime.Now;
            venda.ItensVenda = itemVenda;
            await _db.Vendas.AddAsync(venda);
            await _db.SaveChangesAsync();
        }

        public async Task<List<VendaGet>> GetAll()
        {
            var vendas = await _db.Vendas
                .Include(v => v.StatusVenda)
                .Include(v => v.Vendedor)
                .ToListAsync();
            return _mapper.Map<List<VendaGet>>(vendas);
        }

        public async Task<VendaGet> GetById(int id)
        {
            var venda = await GetDetailedVenda(id);
            return _mapper.Map<VendaGet>(venda);
        }

        public async Task UpdateStatusVenda(VendaUpdateStatus model)
        {
            // Validação dos dados
            Venda venda = await GetVenda(model.VendaId);
            if ((model.StatusVendaId <= 0 || model.StatusVendaId >= 6) ||
               !(
                (venda.StatusVendaId == 1 && model.StatusVendaId == 2) ||
                (venda.StatusVendaId == 1 && model.StatusVendaId == 5) ||
                (venda.StatusVendaId == 2 && model.StatusVendaId == 3) ||
                (venda.StatusVendaId == 2 && model.StatusVendaId == 5) ||
                (venda.StatusVendaId == 3 && model.StatusVendaId == 4)))
            {
                throw new AppException("Alteração do Status Não Permitida!!");
            }

            // Altera o Status da venda
            venda.StatusVendaId = model.StatusVendaId;
            _db.Vendas.Update(venda);
            await _db.SaveChangesAsync();
        }

        private async Task<Venda> GetVenda(int id)
        {
            var venda = await _db.Vendas.FindAsync(id);
            if (venda == null) throw new KeyNotFoundException("Venda não Localizada");
            return venda;
        }

        private async Task<Venda> GetDetailedVenda(int id)
        {
            var venda = await _db.Vendas.Where(v => v.Id == id)
                .Include(v => v.StatusVenda)
                .Include(v => v.Vendedor)
                .Include(v => v.ItensVenda)
                .ThenInclude(iv => iv.Produto)
                .FirstOrDefaultAsync();
            if (venda == null) throw new KeyNotFoundException("Venda não Localizada");
            return venda;
        }
    }
}