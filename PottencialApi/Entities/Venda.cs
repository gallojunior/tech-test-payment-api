namespace PottencialApi.Entities;

public class Venda
{
    public int Id { get; set; }
    public int VendedorId { get; set; }
    public Vendedor Vendedor { get; set; }
    public int StatusVendaId { get; set; }
    public StatusVenda StatusVenda { get; set; }
    public DateTime DataVenda { get; set; }
    public ICollection<ItemVenda> ItensVenda { get; set; }
}
