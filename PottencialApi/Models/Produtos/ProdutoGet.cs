namespace PottencialApi.Models.Produtos;

public class ProdutoGet
{
    public int Id { get; set; }
    public string Nome { get; set; }
    public decimal Valor { get; set; }
}
