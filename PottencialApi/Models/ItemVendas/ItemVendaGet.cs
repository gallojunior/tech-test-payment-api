using PottencialApi.Models.Produtos;

namespace PottencialApi.Models.ItemVendas;

public class ItemVendaGet
{
    public int Id { get; set; }
    public int VendaId { get; set; }
    public ProdutoGet Produto { get; set; }
    public int Qtde { get; set; }
}
