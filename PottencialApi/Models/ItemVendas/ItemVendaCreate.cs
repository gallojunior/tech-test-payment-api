using System.ComponentModel.DataAnnotations;

namespace PottencialApi.Models.ItemVendas;

public class ItemVendaCreate
{
    [Required]
    public int ProdutoId { get; set; }

    [Required]
    [Range(1, 10000)]
    public int Qtde { get; set; }
}
