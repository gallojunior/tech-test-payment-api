namespace PottencialApi.Models.StatusVendas;

public class StatusVendaGet
{
    public int Id { get; set; }
    public string Nome { get; set; }
}
