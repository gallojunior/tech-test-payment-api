using System.ComponentModel.DataAnnotations;
using PottencialApi.Models.ItemVendas;

namespace PottencialApi.Models.Vendas;

public class VendaCreate
{
    [Required]
    public int VendedorId { get; set; }

    public ICollection<ItemVendaCreate> ItemVenda { get; set; }
}
