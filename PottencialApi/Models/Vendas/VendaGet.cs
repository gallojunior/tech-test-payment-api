using PottencialApi.Models.ItemVendas;
using PottencialApi.Models.StatusVendas;
using PottencialApi.Models.Vendedores;

namespace PottencialApi.Models.Vendas;

public class VendaGet
{
    public int Id { get; set; }
    public int VendedorId { get; set; }
    public VendedorGet Vendedor { get; set; }
    public int StatusVendaId { get; set; }
    public StatusVendaGet StatusVenda { get; set; }
    public DateTime DataVenda { get; set; }
    public ICollection<ItemVendaGet> ItensVenda { get; set; }
}
