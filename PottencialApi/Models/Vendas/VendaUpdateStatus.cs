using System.ComponentModel.DataAnnotations;

namespace PottencialApi.Models.Vendas;

public class VendaUpdateStatus
{
    [Required]
    public int VendaId { get; set; }

    [Required]
    public int StatusVendaId { get; set; }
}
