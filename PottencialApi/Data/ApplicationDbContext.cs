using Microsoft.EntityFrameworkCore;
using PottencialApi.Entities;

namespace PottencialApi.Data;

public class ApplicationDbContext : DbContext
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
    {
    }

    public DbSet<ItemVenda> ItemVendas { get; set; }
    public DbSet<Produto> Produtos { get; set; }
    public DbSet<StatusVenda> StatusVendas { get; set; }
    public DbSet<Venda> Vendas { get; set; }
    public DbSet<Vendedor> Vendedores { get; set; }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);

        #region Seed StatusVenda
        List<StatusVenda> listStatusVenda = new()
            {
                new StatusVenda(){
                    Id = 1,
                    Nome = "Aguardando pagamento"
                },
                new StatusVenda(){
                    Id = 2,
                    Nome = "Pagamento Aprovado"
                },
                new StatusVenda(){
                    Id = 3,
                    Nome = "Enviado para Transportadora"
                },
                new StatusVenda(){
                    Id = 4,
                    Nome = "Entregue"
                },
                new StatusVenda(){
                    Id = 5,
                    Nome = "Cancelada"
                }
            };
        builder.Entity<StatusVenda>().HasData(listStatusVenda);
        #endregion

        #region Seed Vendedor
        List<Vendedor> listVendedor = new()
            {
                new Vendedor(){
                    Id = 1,
                    Nome = "João Maria",
                    Cpf = "12345678901",
                    Email = "joao@shop.com.br",
                    Telefone = "11999998888"
                },
                new Vendedor(){
                    Id = 2,
                    Nome = "José Pedro",
                    Cpf = "98765432109",
                    Email = "jose@shop.com.br",
                    Telefone = "11888889999"
                }
            };
        builder.Entity<Vendedor>().HasData(listVendedor);
        #endregion

        #region Seed Produto
        List<Produto> listProduto = new()
            {
                new Produto(){
                    Id = 1,
                    Nome = "Mouse",
                    Valor = 30
                },
                new Produto(){
                    Id = 2,
                    Nome = "Teclado",
                    Valor = 80
                },
                new Produto(){
                    Id = 3,
                    Nome = "Monitor",
                    Valor = 600
                },
                new Produto(){
                    Id = 4,
                    Nome = "Gabinete",
                    Valor = 200
                },
                new Produto(){
                    Id = 5,
                    Nome = "Fonte",
                    Valor = 90
                },
                new Produto(){
                    Id = 6,
                    Nome = "Placa-mãe",
                    Valor = 500
                },
                new Produto(){
                    Id = 7,
                    Nome = "Processador",
                    Valor = 800
                }
            };
        builder.Entity<Produto>().HasData(listProduto);
        #endregion


        Venda venda = new Venda()
        {
            Id = 1,
            DataVenda = DateTime.Now,
            StatusVendaId = 1,
            VendedorId = 1,
        };
        builder.Entity<Venda>().HasData(venda);

        ItemVenda itemVenda = new ItemVenda()
        {
            Id = 1,
            VendaId = 1,
            ProdutoId = 1,
            Qtde = 2
        };
        builder.Entity<ItemVenda>().HasData(itemVenda);
    }
}
