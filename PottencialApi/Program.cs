using Microsoft.EntityFrameworkCore;
using PottencialApi.Data;
using PottencialApi.Helpers;
using PottencialApi.Services.Vendas;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();
builder.Services.AddDbContext<ApplicationDbContext>(options =>
    options.UseInMemoryDatabase("PottencialApiDb"));
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

// Configuração da Injeção de Dependência do Serviço de Vendas
builder.Services.AddScoped<IVendaService, VendaService>();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();


// Código para replicação dos dados do contexto em memória - código a ser excluído ao ser implementado banco de dados local ou remoto
ApplicationDbContext dbContexto = app.Services.CreateAsyncScope()
    .ServiceProvider.GetRequiredService<ApplicationDbContext>();
await dbContexto.Database.EnsureCreatedAsync();


// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

// Global error handler
app.UseMiddleware<ErrorHandlerMiddleware>();

app.MapControllers();

app.Run();
