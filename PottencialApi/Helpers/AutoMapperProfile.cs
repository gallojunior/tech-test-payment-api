using AutoMapper;
using PottencialApi.Entities;
using PottencialApi.Models.ItemVendas;
using PottencialApi.Models.Produtos;
using PottencialApi.Models.StatusVendas;
using PottencialApi.Models.Vendas;
using PottencialApi.Models.Vendedores;

namespace PottencialApi.Helpers;

public class AutoMapperProfile : Profile
{
    public AutoMapperProfile()
    {
        CreateMap<ItemVendaCreate, ItemVenda>().ReverseMap();
        CreateMap<ItemVendaGet, ItemVenda>().ReverseMap();

        CreateMap<ProdutoGet, Produto>().ReverseMap();

        CreateMap<StatusVendaGet, StatusVenda>().ReverseMap();

        CreateMap<VendaGet, Venda>().ReverseMap();
        CreateMap<VendaCreate, Venda>().ReverseMap();
        CreateMap<VendaUpdateStatus, Venda>().ReverseMap();

        CreateMap<VendedorGet, Vendedor>().ReverseMap();
    }
}
